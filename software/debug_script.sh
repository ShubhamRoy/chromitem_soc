#!/bin/bash

set -e
set -o pipefail

unset elf
unset cfg
unset openocd
unset gdb
while [[ "$1" != "" ]]
do
    case "$1"
    in
    --elf)                 elf="$2";     shift 2;;
    --openocd-config)      cfg="$2";     shift 2;;
    *) echo "$0: Unknown argument $1";   exit 1;;
    esac
done

openocd -f $cfg &
riscv64-unknown-elf-gdb $elf -ex "set remotetimeout 500" -ex "target remote localhost:3333" \
  -ex "set pagination off" -ex "load"

kill %1
