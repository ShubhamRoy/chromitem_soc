// Copyright (c) 2021 InCore Semiconductors Pvt. Ltd.
// see LICENSE.incore for more details on licensing terms
/*
Author: Babu P S , babu.ps@incoresemi.com
Created on: Thursday 15 April 2021 11:58:16 AM IST
Description: Copy application from flash memory to physical memory
*/
#include <stdio.h>

typedef struct {
   uint8_t dplx : 1;
   uint8_t lsb  : 1;
   uint8_t cpha : 1;
   uint8_t cpol : 1;
} spi_setup;

//pointers to register

int* spi_cr1    = (int*) 0x00020100;
int* spi_cr2    = (int*) 0x00020104;
int* spi_en     = (int*) 0x00020108;
int* spi_txr    = (int*) 0x00020110;
int* spi_pscr   = (int*) 0x00020124;
// DO NOT CHANGE THE BELOW VARIABLE TYPES
volatile uintptr_t* spi_sr = (uintptr_t*)   0x0002010C;
unsigned int* spi_rxr    = (unsigned int *) 0x00020114;

// SOME CONSTANTS USED IN FLASH
#define SPI_CLR_ST              (1 << 10)
#define RXNE                    (1 << 0)
#define TXE                     (1 << 1)
#define SPI_TX_FIFO_NF          (1 << 3)

void set_spi(int* addr, int val){
    *addr = val;
}

int get_spi(int* addr){
    return *addr;
}

int bitExtracted(int number, int k, int p)
{
    return (((1 << k) - 1) & (number >> (p - 1)));
}

void set_bits_rx_tx(uint8_t tx_bits, uint8_t rx_bits){
    volatile uint32_t sreg = get_spi(spi_cr1);
    sreg &= 0x0000FFFF ;
    set_spi(spi_cr1, (sreg | SPI_TOTAL_BITS_TX(tx_bits) | SPI_TOTAL_BITS_RX(rx_bits)));
}

uint8_t write_spi(uint32_t write_val){
    uint32_t spi_status = get_spi(spi_sr);
    if (spi_status & SPI_TX_FIFO_NF){        // Check if TxFIFO is not FUll
        while(! (spi_status & TXE)){        // Wait until TxR is available
             waitfor(10);
        }
        set_spi(spi_txr, write_val);
        return 1;
    }
    else {
        printf("Tx FIFO is full with %d bytes\n",((spi_status & 0x00FF0000) >> 16));
        return 0;
    }
}

uint32_t read_spi(uint32_t delay){
    while(! (get_spi(spi_sr) & RXNE)){ // Wait until RxR is available
        waitfor(delay);
    }
    return *spi_rxr;
}

void spi_init(uint8_t slave_select, spi_setup dml , uint16_t prescalar){
    set_spi(spi_pscr, prescalar);
    set_spi(spi_cr1, (SPI_DPLX(dml.dplx) | SPI_CLR_ST | SPI_IS_RX_1ST(0) |
            SPI_LSBFIRST(dml.lsb) | SPI_CPHA(dml.cpol) | SPI_CPHA(dml.cpha)));
    set_spi(spi_cr2, (SPI_SLV_SEL(slave_select)));
}

uint32_t flash_device_id(){
    uint32_t readVal;
    set_bits_rx_tx(8 , 24);
    write_spi(0x9F000000);
    set_spi(spi_en, 1);
    return read_spi(100);
}

uint32_t flash_single_read(uint32_t addr){
    uint32_t cmd_addr = 0x0C000000 | bitExtracted(addr, 24, 9);
    uint32_t address = bitExtracted(addr, 8, 1) << 24;
    // command (8) + addr (32) + dummy(CR2NV[3:0] = 8)
    set_bits_rx_tx(48 , 32);
    write_spi(cmd_addr);
    write_spi(address);
    set_spi(spi_en, 1);
    return read_spi(0);
}

/******************************* WARNING: **********************************/
/* USAGE OF THIS API IS NOT RECOMMENDED FOR DRIVERS and IS RESTRICTED TO FLASH
BLOCK READ ONLY. AS THIS API IS PRONE TO READING BEYOND SPECIFIED ADDRESS DUE TO
INHERENT LOAD / STORE DELAYS, PARTICULARLY AT FASTER CLOCK RATES (Prescalar < 16).
****************************************************************************
Description: block read Specific for reading flash at half bus-clock speeds.

 This function is written in a way that compiler will reduce intermediate load
 and stores and is not recommended for generic usage. (Unless the developer is
 aware of the side-effects). This function will read beyond the specified wr_addr
 limit from the flash as the 'spi_en = 0' reaches late to SPI Peripheral
*/
uint8_t flash_block_read(uint32_t rd_addr, uint32_t* wr_addr, const unsigned int sz){
    register unsigned int limit = sz;
    register unsigned int indx = 0;
    register uint32_t *lv_wr_addr = wr_addr;
    register unsigned int* rxreg = spi_rxr;
    register unsigned int* srreg = spi_sr;
    uint32_t cmd_addr = 0x0C000000 | bitExtracted(rd_addr, 24, 9);
    uint32_t address = bitExtracted(rd_addr, 8, 1) << 24;
    // command (8) + addr (32) + dummy(CR2NV[3:0] = 8)
    // Receive unlimited data continously
    set_bits_rx_tx(48 , 255);
    write_spi(cmd_addr);
    write_spi(address);
    set_spi(spi_en, 1);
    while( indx++ < limit) {
    // The delay inside should be lowest when prescalar is smallest, for
    // larger prescalar keeping small delay will create unnecessary traffic on bus.
    // ideal delay value = prescalar * 4;
    while(!(*srreg & 1)); /*{
        waitfor(delay);
    }*/
    *(lv_wr_addr++) = (unsigned int)*rxreg;
    }
    set_spi(spi_en, 0);
    return 1;
}

void jumpToRAM(){
    printf("\n%s\n","Control transferred to RAM");
    asm volatile("fence.i");
    asm volatile( "li x30, 0x80000000" "\n\t"
            "jr x30" "\n\t"
            :
            :
            :"x30","cc","memory"

            );
}

void main(){
    uint32_t read_address;  // read/write from/to this address
    uint32_t* bram_address = (uint32_t*) 0x80000000;
    uint32_t sze;
    uint32_t arch = 0;

    asm volatile(
             "csrr %[arch], marchid\n"
             :
             [arch]
             "=r"
             (arch)
            );

    printf("%s\n\nINCORE SEMICONDUCTORS Pvt. Ltd. \n\n",bootlogo);
    printf("%s\n","Booting... vspi1.0\n");

    printf("Booting %s ! hart 0 \n", soc_name);
    printf("%s",VERSIONVAL);
    printf("%s",BUILDVAL);
    printf("%s",copyright);
    printf("%s",license);
    printf("\n");
    printf("Supported ISA: RV64IMACSU.\n");
    printf("Processor Arch ID: %x.\n", arch);

    // duplex disabled, Tx first in Half duplex, CPHA, CPOL
    spi_setup dml = {0, 0, 0, 0};
    spi_init(0, dml, 2);

    uint8_t mfg_id = ((flash_device_id() & 0xFF000000) >> 24) ;

    if(1 == mfg_id ){
        read_address = 0x00b00000;
        sze = flash_single_read(read_address);

        if(sze == 0 || sze == 0xFFFFFFFF){
            printf("No content to boot \n");
            asm volatile ("ebreak");
        }

	    read_address = read_address + 4;
        flash_block_read(read_address, bram_address, sze); // Performs fastest possible read

/*  //  Performs Slower reads each word with 40 bits overhead, but gives a indication for 4KB read data
        for(unsigned int i = 0; i < sze; i++ ){
            read_value        = flash_single_read(read_address);
            *(bram_address++) = read_value;
            read_address      = read_address+4;

            if(i%1024 == 0)
                printf(".");
        }
*/
    }
    else{
        printf("Wrong device id \n");

        asm volatile ("ebreak");
    }

    jumpToRAM();

    asm volatile ("ebreak");
}

