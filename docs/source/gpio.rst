.. _gpio:

#######################################
General Purpose Input Output Controller
#######################################

This chapter provides details of the General Purpose Input Output (GPIO) controller instantiated in the design.
The GPIO controller is a memory mapped device and provides a low-level software configruation
of the actual general purpose IO cells/pads available in this design.

IP Details and Available Configuration
======================================


:numref:`GPIO_ip_details` provides details of the source of the IP and and
details of the memory map.

.. tabularcolumns:: |l|C|

.. _GPIO_ip_details:

.. table:: GPIO IP details

  ========================================  ==============
  ..                                        **Value**
  ========================================  ==============
  Provider                                  gitlab
  Vendor                                    incoresemi
  Library                                   blocks/devices
  Version                                   1.1.2
  Ip Type                                   memory_mapped
  Numer of Config Registers                 19
  Direct Memory Region                      None
  Configuration Register Alignment (bytes)  4
  ========================================  ==============

:numref:`GPIO_configuration_details` provides information of the various
parameters of the IP available at design time and their description

.. tabularcolumns:: |l|l|l|

.. _GPIO_configuration_details:

.. table:: GPIO IP Configuration Options

  ===============  ========================  ======================================================================================================================================================================================
  Configuration    Options                   Description
  ===============  ========================  ======================================================================================================================================================================================
  Bus interfaces   APB, AXI4L, AXI4          Choice of bus interface protocol supported on this IP
  Base address     Integer                   The base address where the memory map of the configuration register starts
  Bytes reserved   Integer >= 0X4C           The number of bytes reserved for this instance. This can be much larger than the actual bytes required for the configuration registers but cannot be smaller than the value indicated.
  ionum            Integer > 1 and  < 32     defines the number of GPIOs to be controlled by the instance. Must be >1 and < 32.
  interrupt_size   Integer > 0 and  < ionum  defines the number of lower order GPIO pins that can generate an interrupt. Must be >=0 and <= ionum.
  ===============  ========================  ======================================================================================================================================================================================



GPIO Instance Details
=====================



:numref:`GPIO_instance_details` shows the values assigned to parameters of this
instance of the IP.

.. tabularcolumns:: |c|C|

.. _GPIO_instance_details:

.. table:: GPIO Instance Parameters and Assigned Values

  ====================  ====================
  **Parameter Name**    **Value Assigned**
  ====================  ====================
  Base Address          0X20000
  Bound Address         0X200FF
  Bytes reserved        0XFF
  Bus Interface         APB
  ionum                 0X16
  interrupt_size        0X10
  ====================  ====================


Register Map
============



The register map for the GPIO control registers is shown in
:numref:`GPIO_register_map`. 

.. tabularcolumns:: |l|c|c|c|l|

.. _GPIO_register_map:

.. table:: GPIO Register Mapping for all configuration registes

  +-----------------+---------------+--------------+--------------+------------------------------------------------------------------------------------------------+
  | Register-Name   | Offset(hex)   |   Size(Bits) | Reset(hex)   | Description                                                                                    |
  +=================+===============+==============+==============+================================================================================================+
  | input_val       | 0X0           |           22 | 0X0          | This register holds the IO cell input pin value                                                |
  +-----------------+---------------+--------------+--------------+------------------------------------------------------------------------------------------------+
  | input_enable    | 0X4           |           22 | 0X0          | This register drives the input enable pin of the IO cell                                       |
  +-----------------+---------------+--------------+--------------+------------------------------------------------------------------------------------------------+
  | output_val      | 0X8           |           22 | 0X0          | This register drives the output value pin of the IO cell                                       |
  +-----------------+---------------+--------------+--------------+------------------------------------------------------------------------------------------------+
  | output_en       | 0XC           |           22 | 0X0          | This register drives the output enable pin of the IO cell                                      |
  +-----------------+---------------+--------------+--------------+------------------------------------------------------------------------------------------------+
  | pullup_en       | 0X10          |           22 | 0X0          | Controls the internal pull of the respective IO cells                                          |
  +-----------------+---------------+--------------+--------------+------------------------------------------------------------------------------------------------+
  | drive_0         | 0X14          |           22 | 0X0          | Controls the drive strength of the respective IO cells                                         |
  +-----------------+---------------+--------------+--------------+------------------------------------------------------------------------------------------------+
  | rise_ie         | 0X18          |           22 | 0X0          | Enables interrupt when the input val of the IO cell sees a rising edge                         |
  +-----------------+---------------+--------------+--------------+------------------------------------------------------------------------------------------------+
  | rise_ip         | 0X1C          |           22 | 0X0          | Indicates a pending interrupt due a rising edge detected on the respective IO cell             |
  +-----------------+---------------+--------------+--------------+------------------------------------------------------------------------------------------------+
  | fall_ie         | 0X20          |           22 | 0X0          | Enables interrupt when the input val of the IO cell sees a falling edge                        |
  +-----------------+---------------+--------------+--------------+------------------------------------------------------------------------------------------------+
  | fall_ip         | 0X24          |           22 | 0X0          | Indicates a pending interrupt due a rising edge detected on the respective IO cell             |
  +-----------------+---------------+--------------+--------------+------------------------------------------------------------------------------------------------+
  | high_ie         | 0X28          |           22 | 0X0          | Enables interrupt when the input val of the IO cell sees a value of 1                          |
  +-----------------+---------------+--------------+--------------+------------------------------------------------------------------------------------------------+
  | high_ip         | 0X2C          |           22 | 0X0          | Indicates a pending interrupt due a 1 being detected on the respective IO cell                 |
  +-----------------+---------------+--------------+--------------+------------------------------------------------------------------------------------------------+
  | low_ie          | 0X30          |           22 | 0X0          | Enables interrupt when the input val of the IO cell sees a value of 0                          |
  +-----------------+---------------+--------------+--------------+------------------------------------------------------------------------------------------------+
  | low_ip          | 0X34          |           22 | 0X0          | Indicates a pending interrupt due a 0 being detected on the respective IO cell                 |
  +-----------------+---------------+--------------+--------------+------------------------------------------------------------------------------------------------+
  | set_out         | 0X38          |           22 | 0X0          | Attempts to drive the output of IO cell high irrespective of the value of output_val register. |
  +-----------------+---------------+--------------+--------------+------------------------------------------------------------------------------------------------+
  | clr_out         | 0X3C          |           22 | 0X0          | Attempts to drive the output of IO cell low irrespective of the value of output_val register.  |
  +-----------------+---------------+--------------+--------------+------------------------------------------------------------------------------------------------+
  | xor_out         | 0X40          |           22 | 0X0          | Attempts to drive the IO cell with the inverted value of the output_val register.              |
  +-----------------+---------------+--------------+--------------+------------------------------------------------------------------------------------------------+
  | drive_1         | 0X44          |           22 | 0X0          | Extra bits to control the drive strength of the respective IO cells                            |
  +-----------------+---------------+--------------+--------------+------------------------------------------------------------------------------------------------+
  | drive_2         | 0X48          |           22 | 0X0          | Extra bits to control the drive strength of the respective IO cells                            |
  +-----------------+---------------+--------------+--------------+------------------------------------------------------------------------------------------------+

All addresses not mentioned in the above table within ``Base Address`` and
``Bound Address`` are reserved and accesses to those regions will generate a
slave error on the bus interface



If ``ionum`` parameter is less than 32, then the upper bits of the registers
are not used i.e. writes will have no effect on them and will always be read as
zeros



The register access attributes for the GPIO control registers are shown in 
:numref:`GPIO_register_access_attr`.

.. tabularcolumns:: |l|c|c|c|C|

.. _GPIO_register_access_attr:

.. table:: GPIO Register Access Attributes for all configuration Registers

  ===============  =============  ============  ============  ============
  Register-Name    Access Type    Reset Type    Min Access    Max Access
  ===============  =============  ============  ============  ============
  input_val        read-only      synchronous   1B            4B
  input_enable     read-write     synchronous   1B            4B
  output_val       read-write     synchronous   1B            4B
  output_en        read-write     synchronous   1B            4B
  pullup_en        read-write     synchronous   1B            4B
  drive_0          read-write     synchronous   1B            4B
  rise_ie          read-write     synchronous   1B            4B
  rise_ip          read-write     synchronous   1B            4B
  fall_ie          read-write     synchronous   1B            4B
  fall_ip          read-write     synchronous   1B            4B
  high_ie          read-write     synchronous   1B            4B
  high_ip          read-write     synchronous   1B            4B
  low_ie           read-write     synchronous   1B            4B
  low_ip           read-write     synchronous   1B            4B
  set_out          read-write     synchronous   1B            4B
  clr_out          read-write     synchronous   1B            4B
  xor_out          read-write     synchronous   1B            4B
  drive_1          read-write     synchronous   1B            4B
  drive_2          read-write     synchronous   1B            4B
  ===============  =============  ============  ============  ============

:numref:`GPIO_register_sideeffects` captures the side-effects caused to either reads or writes
on certain registers.

.. tabularcolumns:: |l|l|l|

.. _GPIO_register_sideeffects:

.. table:: GPIO Register Side Effects

  ===============  ===================  ============================================================
  Register-Name    Read Side Effects    Write Side Effects
  ===============  ===================  ============================================================
  rise_ip          none                 writing a value of 1 clears the respective pending interrupt
  fall_ip          none                 writing a value of 1 clears the respective pending interrupt
  high_ip          none                 writing a value of 1 clears the respective pending interrupt
  low_ip           none                 writing a value of 1 clears the respective pending interrupt
  ===============  ===================  ============================================================

.. note:: Registers not included in the Side Effects table have no side-effects generated
  either on a read or a write.






INPUT_VAL Register
==================

This register holds the IO cell input pin value

.. bitfield::
    :bits: 22
    :lanes: 1
    :fontsize: 10
    :vspace: 50
    :hspace: 1200

    [
    {"bits": 22, "name": "INPUT_VAL", "attr":"read-only" }]


INPUT_ENABLE Register
=====================

This register drives the input enable pin of the IO cell

.. bitfield::
    :bits: 22
    :lanes: 1
    :fontsize: 10
    :vspace: 50
    :hspace: 1200

    [
    {"bits": 22, "name": "INPUT_ENABLE", "attr":"read-write" }]


OUTPUT_VAL Register
===================

This register drives the output value pin of the IO cell

.. bitfield::
    :bits: 22
    :lanes: 1
    :fontsize: 10
    :vspace: 50
    :hspace: 1200

    [
    {"bits": 22, "name": "OUTPUT_VAL", "attr":"read-write" }]


OUTPUT_EN Register
==================

This register drives the output enable pin of the IO cell

.. bitfield::
    :bits: 22
    :lanes: 1
    :fontsize: 10
    :vspace: 50
    :hspace: 1200

    [
    {"bits": 22, "name": "OUTPUT_EN", "attr":"read-write" }]


PULLUP_EN Register
==================

Controls the internal pull of the respective IO cells

.. bitfield::
    :bits: 22
    :lanes: 1
    :fontsize: 10
    :vspace: 50
    :hspace: 1200

    [
    {"bits": 22, "name": "PULLUP_EN", "attr":"read-write" }]


DRIVE_0 Register
================

Controls the drive strength of the respective IO cells

.. bitfield::
    :bits: 22
    :lanes: 1
    :fontsize: 10
    :vspace: 50
    :hspace: 1200

    [
    {"bits": 22, "name": "DRIVE_0", "attr":"read-write" }]


RISE_IE Register
================

Enables interrupt when the input val of the IO cell sees a rising edge

.. bitfield::
    :bits: 22
    :lanes: 1
    :fontsize: 10
    :vspace: 50
    :hspace: 1200

    [
    {"bits": 22, "name": "RISE_IE", "attr":"read-write" }]


RISE_IP Register
================

Indicates a pending interrupt due a rising edge detected on the respective IO cell

.. bitfield::
    :bits: 22
    :lanes: 1
    :fontsize: 10
    :vspace: 50
    :hspace: 1200

    [
    {"bits": 22, "name": "RISE_IP", "attr":"read-write" }]


FALL_IE Register
================

Enables interrupt when the input val of the IO cell sees a falling edge

.. bitfield::
    :bits: 22
    :lanes: 1
    :fontsize: 10
    :vspace: 50
    :hspace: 1200

    [
    {"bits": 22, "name": "FALL_IE", "attr":"read-write" }]


FALL_IP Register
================

Indicates a pending interrupt due a rising edge detected on the respective IO cell

.. bitfield::
    :bits: 22
    :lanes: 1
    :fontsize: 10
    :vspace: 50
    :hspace: 1200

    [
    {"bits": 22, "name": "FALL_IP", "attr":"read-write" }]


HIGH_IE Register
================

Enables interrupt when the input val of the IO cell sees a value of 1

.. bitfield::
    :bits: 22
    :lanes: 1
    :fontsize: 10
    :vspace: 50
    :hspace: 1200

    [
    {"bits": 22, "name": "HIGH_IE", "attr":"read-write" }]


HIGH_IP Register
================

Indicates a pending interrupt due a 1 being detected on the respective IO cell

.. bitfield::
    :bits: 22
    :lanes: 1
    :fontsize: 10
    :vspace: 50
    :hspace: 1200

    [
    {"bits": 22, "name": "HIGH_IP", "attr":"read-write" }]


LOW_IE Register
===============

Enables interrupt when the input val of the IO cell sees a value of 0

.. bitfield::
    :bits: 22
    :lanes: 1
    :fontsize: 10
    :vspace: 50
    :hspace: 1200

    [
    {"bits": 22, "name": "LOW_IE", "attr":"read-write" }]


LOW_IP Register
===============

Indicates a pending interrupt due a 0 being detected on the respective IO cell

.. bitfield::
    :bits: 22
    :lanes: 1
    :fontsize: 10
    :vspace: 50
    :hspace: 1200

    [
    {"bits": 22, "name": "LOW_IP", "attr":"read-write" }]


SET_OUT Register
================

Attempts to drive the output of IO cell high irrespective of the value of output_val register.

.. bitfield::
    :bits: 22
    :lanes: 1
    :fontsize: 10
    :vspace: 50
    :hspace: 1200

    [
    {"bits": 22, "name": "SET_OUT", "attr":"read-write" }]


CLR_OUT Register
================

Attempts to drive the output of IO cell low irrespective of the value of output_val register.

.. bitfield::
    :bits: 22
    :lanes: 1
    :fontsize: 10
    :vspace: 50
    :hspace: 1200

    [
    {"bits": 22, "name": "CLR_OUT", "attr":"read-write" }]


XOR_OUT Register
================

Attempts to drive the IO cell with the inverted value of the output_val register.

.. bitfield::
    :bits: 22
    :lanes: 1
    :fontsize: 10
    :vspace: 50
    :hspace: 1200

    [
    {"bits": 22, "name": "XOR_OUT", "attr":"read-write" }]


DRIVE_1 Register
================

Extra bits to control the drive strength of the respective IO cells

.. bitfield::
    :bits: 22
    :lanes: 1
    :fontsize: 10
    :vspace: 50
    :hspace: 1200

    [
    {"bits": 22, "name": "DRIVE_1", "attr":"read-write" }]


DRIVE_2 Register
================

Extra bits to control the drive strength of the respective IO cells

.. bitfield::
    :bits: 22
    :lanes: 1
    :fontsize: 10
    :vspace: 50
    :hspace: 1200

    [
    {"bits": 22, "name": "DRIVE_2", "attr":"read-write" }]


Input/Output Values
===================

The GPIOs can be configured in a bit-wise fashion, allowing any pad to act as an input or output
by controlling the ``input_enable`` and ``output_enable`` registers. The effect of setting both is
defined by the PADs. The values written to the
``output_val`` registers are reflected directly on the GPIO pads. The input values from the GPIO
pads is received in the ``input_val`` register after 2 consecutive cycles if the input hasn't
changed.


Interrupts
==========

Any of the ``interrupt_size`` lower order GPIOs pins can generate an interrupt bit. The controller supports generation of
four different kind of interrupts:

1. rise interrupts: These interrupts are generated when the corresponding GPIO bit in ``input_val``
   detects a rising edge and the correspdoning bit in ``rise_ie`` is set.
2. fall interrupts: These interrupts are generated when the corresponding GPIO bit in ``input_val``
   detects a falling edge and the correspdoning bit in ``fall_ie`` is set.
3. high interrupts: These interrupts are generated when the corresponding GPIO bit in ``input_val``
   if held high and the correspdoning bit in ``high_ie`` is set.
4. low interrupts: These interrupts are generated when the corresponding GPIO bit in ``input_val``
   if held low and the correspdoning bit in ``low_ie`` is set.

Since the inputs are synchronized before being sampled by the interrupt logic, it is expected that
input pulse width at the GPIO pads is long enough to be detected by the synchronization logic.

.. note:: A single GPIO can be configured to detect none, any or all types of the above defined interrupts
  by setting the appropriate bits in the \*_ie regsiters of the IP.

Once the interrupt is pending it will remain set until a 1 is written to the appropriate \*_ip
registers at the corresponding bit location.

These interrupt pins can be routed to the PLIC, or connected to local interrupts of the hart.

.. note:: Although controller updates the interrupt registers for all ``GPIO[ionum:0]`` only ``GPIO[interrupt_size:0]`` are routed to PLIC.


Output Logic Locking
====================

The GPIO pads' output values can be locked to high, low or track an inverted version of the ``output_val``
by controlling the registers ``set_out``, ``clr_out`` and ``xor_out`` respectively.
It is possible to set the same bit location in all the above registers, in which case the GPIO pad
is driven the following logic:

.. code:: verilog

  gpio_pad_out =  ( ( (output_val & ~clr_out ) | set_out ) ^ xor_out )


IO and Sideband Signals
=======================



The following table describes the io-signals generated from this IP that may
directly or indirectly drive certain IO pads.

.. tabularcolumns:: |l|l|l|l|

.. _GPIO_io_signals:

.. table:: GPIO generated IO signals

  ===================  ======  ===========  =========================================================
  Signal Name (RTL)      Size  Direction    Description
  ===================  ======  ===========  =========================================================
  gpio_in_en               22  output       connected to the input_enable of the IO cells
  gpio_out_val             22  output       connected to the output_value of the IO cells
  gpio_out_en              22  output       connected to the output_enable of the IO cells
  gpio_pull_en             22  output       connected to the internal pull up control of the IO cells
  gpio_drive_0             22  output       connected to the drive strength control of the IO cells
  gpio_drive_1             22  output       connected to the drive strength control of the IO cells
  gpio_drive_2             22  output       connected to the drive strength control of the IO cells
  gpio_in_val              22  input        connected to the input value of the IO cell
  ===================  ======  ===========  =========================================================

.. note:: some of these signals may be muxed with other functional IO from different
  ips and users should refer to any pinmux module available on chip





.. tabularcolumns:: |l|l|l|l|

.. _GPIO_sb_signals:

.. table:: GPIO generated side-band signals generated

  ===================  ======  ===========  =====================================================================
  Signal Name (RTL)      Size  Direction    Description
  ===================  ======  ===========  =====================================================================
  sb_gpio_to_plic          16  output       Interrupt signals. Some or all of these can be connected to the PLIC.
  ===================  ======  ===========  =====================================================================

