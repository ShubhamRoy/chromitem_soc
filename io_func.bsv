// Copyright (c) 2020 InCore Semiconductors Pvt. Ltd. see LICENSE.incore for more details on licensing terms
/*
Author: Neel Gala
Email id: neelgala@gmail.com
Details:

--------------------------------------------------------------------------------------------------
*/
package io_func;
  `include "Soc_map.bsv"
  // this function is used to indicate the caches which are the non-cacheable regions within the
  // Soc.
  function Bool isIO(Bit#(`paddr) addr, Bool cacheable);
	  if(!cacheable)
		  return True;
    else if(`DDRBase <= addr && addr <= `DDREnd)
      return False;
    else if(`OCMBase <= addr && addr <= `OCMEnd)
      return False;
    else if(`ROMBase <= addr && addr <= `ROMEnd)
      return False;
	  else
		  return True;
  endfunction:isIO
endpackage:io_func

