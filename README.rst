##############
Chromite-M SoC
##############

The Chromite M SoC is amongst InCore’s first No-Cost Eval SoC on FPGA based on the Chromite core 
generator. This SoC is targetted for IoT and Industrial class applications requiring a 64-bit 
micro controller.

The documentation of the project is built using sphinx.
For HTML version click `here <https://chromitem_soc.readthedocs.io>`_


